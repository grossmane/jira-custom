FROM teamatldocker/jira:core.8.9.0.de

USER root

RUN sed -i 's/<session-timeout>300<\/session-timeout>/<session-timeout>0<\/session-timeout>/g' /opt/jira/atlassian-jira/WEB-INF/web.xml

USER jira
CMD ["jira"]
